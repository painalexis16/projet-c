#if defined HAVE_CONFIG_H
#include "config.h"
#endif

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <math.h>
#include <pthread.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "utils.h"
#include "myassert.h"

#include "client_master.h"


/************************************************************************
 * chaines possibles pour le premier paramètre de la ligne de commande
 ************************************************************************/
#define TK_STOP        "stop"             // arrêter le master
#define TK_HOW_MANY    "howmany"          // combien d'éléments dans l'ensemble
#define TK_MINIMUM     "min"              // valeur minimale de l'ensemble
#define TK_MAXIMUM     "max"              // valeur maximale de l'ensemble
#define TK_EXIST       "exist"            // test d'existence d'un élément, et nombre d'exemplaires
#define TK_SUM         "sum"              // somme de tous les éléments
#define TK_INSERT      "insert"           // insertion d'un élément
#define TK_INSERT_MANY "insertmany"       // insertions de plusieurs éléments aléatoires
#define TK_PRINT       "print"            // debug : demande aux master/workers d'afficher les éléments
#define TK_LOCAL       "local"            // lancer un calcul local (sans master) en multi-thread


/************************************************************************
 * structure stockant les paramètres du client
 * - les infos pour communiquer avec le master
 * - les infos pour effectuer le travail (cf. ligne de commande)
 *   (note : une union permettrait d'optimiser la place mémoire)
 ************************************************************************/
typedef struct {
    // communication avec le master
    //TODO
    int client_to_master;
    int master_to_client;
    // infos pour le travail à faire (récupérées sur la ligne de commande)
    int order;     // ordre de l'utilisateur (cf. CM_ORDER_* dans client_master.h)
    float elt;     // pour CM_ORDER_EXIST, CM_ORDER_INSERT, CM_ORDER_LOCAL
    int nb;        // pour CM_ORDER_INSERT_MANY, CM_ORDER_LOCAL
    float min;     // pour CM_ORDER_INSERT_MANY, CM_ORDER_LOCAL
    float max;     // pour CM_ORDER_INSERT_MANY, CM_ORDER_LOCAL
    int nbThreads; // pour CM_ORDER_LOCAL
    int answer;
} Data;


/************************************************************************
 * Usage
 ************************************************************************/
static void usage(const char *exeName, const char *message)
{
    fprintf(stderr, "usages : %s <ordre> [[[<param1>] [<param2>] ...]]\n", exeName);
    fprintf(stderr, "   $ %s " TK_STOP "\n", exeName);
    fprintf(stderr, "          arrêt master\n");
    fprintf(stderr, "   $ %s " TK_HOW_MANY "\n", exeName);
    fprintf(stderr, "          combien d'éléments dans l'ensemble\n");
    fprintf(stderr, "   $ %s " TK_MINIMUM "\n", exeName);
    fprintf(stderr, "          plus petite valeur de l'ensemble\n");
    fprintf(stderr, "   $ %s " TK_MAXIMUM "\n", exeName);
    fprintf(stderr, "          plus grande valeur de l'ensemble\n");
    fprintf(stderr, "   $ %s " TK_EXIST " <elt>\n", exeName);
    fprintf(stderr, "          l'élement <elt> est-il présent dans l'ensemble ?\n");
    fprintf(stderr, "   $ %s " TK_SUM "\n", exeName);
    fprintf(stderr, "           somme des éléments de l'ensemble\n");
    fprintf(stderr, "   $ %s " TK_INSERT " <elt>\n", exeName);
    fprintf(stderr, "          ajout de l'élement <elt> dans l'ensemble\n");
    fprintf(stderr, "   $ %s " TK_INSERT_MANY " <nb> <min> <max>\n", exeName);
    fprintf(stderr, "          ajout de <nb> élements (dans [<min>,<max>[) aléatoires dans l'ensemble\n");
    fprintf(stderr, "   $ %s " TK_PRINT "\n", exeName);
    fprintf(stderr, "          affichage trié (dans la console du master)\n");
    fprintf(stderr, "   $ %s " TK_LOCAL " <nbThreads> <elt> <nb> <min> <max>\n", exeName);
    fprintf(stderr, "          combien d'exemplaires de <elt> dans <nb> éléments (dans [<min>,<max>[)\n"
                    "          aléatoires avec <nbThreads> threads\n");

    if (message != NULL)
        fprintf(stderr, "message :\n    %s\n", message);

    exit(EXIT_FAILURE);
}


/************************************************************************
 * Analyse des arguments passés en ligne de commande
 ************************************************************************/
static void parseArgs(int argc, char * argv[], Data *data)
{
    data->order = CM_ORDER_NONE;

    if (argc == 1)
        usage(argv[0], "Il faut préciser une commande");

    // première vérification : la commande est-elle correcte ?
    if (strcmp(argv[1], TK_STOP) == 0)
        data->order = CM_ORDER_STOP;
    else if (strcmp(argv[1], TK_HOW_MANY) == 0)
        data->order = CM_ORDER_HOW_MANY;
    else if (strcmp(argv[1], TK_MINIMUM) == 0)
        data->order = CM_ORDER_MINIMUM;
    else if (strcmp(argv[1], TK_MAXIMUM) == 0)
        data->order = CM_ORDER_MAXIMUM;
    else if (strcmp(argv[1], TK_EXIST) == 0)
        data->order = CM_ORDER_EXIST;
    else if (strcmp(argv[1], TK_SUM) == 0)
        data->order = CM_ORDER_SUM;
    else if (strcmp(argv[1], TK_INSERT) == 0)
        data->order = CM_ORDER_INSERT;
    else if (strcmp(argv[1], TK_INSERT_MANY) == 0)
        data->order = CM_ORDER_INSERT_MANY;
    else if (strcmp(argv[1], TK_PRINT) == 0)
        data->order = CM_ORDER_PRINT;
    else if (strcmp(argv[1], TK_LOCAL) == 0)
        data->order = CM_ORDER_LOCAL;
    else
        usage(argv[0], "commande inconnue");

    // deuxième vérification : nombre de paramètres correct ?
    if ((data->order == CM_ORDER_STOP) && (argc != 2))
        usage(argv[0], TK_STOP " : il ne faut pas d'argument après la commande");
    if ((data->order == CM_ORDER_HOW_MANY) && (argc != 2))
        usage(argv[0], TK_HOW_MANY " : il ne faut pas d'argument après la commande");
    if ((data->order == CM_ORDER_MINIMUM) && (argc != 2))
        usage(argv[0], TK_MINIMUM " : il ne faut pas d'argument après la commande");
    if ((data->order == CM_ORDER_MAXIMUM) && (argc != 2))
        usage(argv[0], TK_MAXIMUM " : il ne faut pas d'argument après la commande");
    if ((data->order == CM_ORDER_EXIST) && (argc != 3))
        usage(argv[0], TK_EXIST " : il faut un et un seul argument après la commande");
    if ((data->order == CM_ORDER_SUM) && (argc != 2))
        usage(argv[0], TK_SUM " : il ne faut pas d'argument après la commande");
    if ((data->order == CM_ORDER_INSERT) && (argc != 3))
        usage(argv[0], TK_INSERT " : il faut un et un seul argument après la commande");
    if ((data->order == CM_ORDER_INSERT_MANY) && (argc != 5))
        usage(argv[0], TK_INSERT_MANY " : il faut 3 arguments après la commande");
    if ((data->order == CM_ORDER_PRINT) && (argc != 2))
        usage(argv[0], TK_PRINT " : il ne faut pas d'argument après la commande");
    if ((data->order == CM_ORDER_LOCAL) && (argc != 7))
        usage(argv[0], TK_LOCAL " : il faut 5 arguments après la commande");

    // extraction des arguments
    if (data->order == CM_ORDER_EXIST)
    {
        data->elt = strtof(argv[2], NULL);
    }
    else if (data->order == CM_ORDER_INSERT)
    {
        data->elt = strtof(argv[2], NULL);
    }
    else if (data->order == CM_ORDER_INSERT_MANY)
    {
        data->nb = strtol(argv[2], NULL, 10);
        data->min = strtof(argv[3], NULL);
        data->max = strtof(argv[4], NULL);
        if (data->nb < 1)
            usage(argv[0], TK_INSERT_MANY " : nb doit être strictement positif");
        if (data->max < data->min)
            usage(argv[0], TK_INSERT_MANY " : max ne doit pas être inférieur à min");
    }
    else if (data->order == CM_ORDER_LOCAL)
    {
        data->nbThreads = strtol(argv[2], NULL, 10);
        data->elt = strtof(argv[3], NULL);
        data->nb = strtol(argv[4], NULL, 10);
        data->min = strtof(argv[5], NULL);
        data->max = strtof(argv[6], NULL);
        if (data->nbThreads < 1)
            usage(argv[0], TK_LOCAL " : nbThreads doit être strictement positif");
        if (data->nb < 1)
            usage(argv[0], TK_LOCAL " : nb doit être strictement positif");
        if (data->max <= data->min)
            usage(argv[0], TK_LOCAL " : max ne doit être strictement supérieur à min");
    }
}


/************************************************************************
 * Partie multi-thread
 ************************************************************************/
//TODO Une structure pour les arguments à passer à un thread (aucune variable globale autorisée)
typedef struct{
    float *tab;
    int nbElt;
    float elt;
    int *res;
    pthread_mutex_t *mutex;
} Thread;

//TODO
// Code commun à tous les threads
// Un thread s'occupe d'une portion du tableau et compte en interne le nombre de fois
// où l'élément recherché est présent dans cette portion. On ajoute alors,
// en section critique, ce nombre au compteur partagé par tous les threads.
// Le compteur partagé est la variable "result" de "lauchThreads".
// A vous de voir les paramètres nécessaires  (aucune variable globale autorisée)
void* MultiThread(void* args){
    Thread *thread = (Thread*)args;
    int count = 0;
    for(int i=0; i<thread->nbElt; i++){
        if(thread->tab[i] == thread->elt){
            count++;
        }
    }
    pthread_mutex_lock(thread->mutex);
    *(thread->res) += count;
    pthread_mutex_unlock(thread->mutex);

    pthread_exit(NULL);
}
//END TODO

void lauchThreads(const Data *data)
{
    //TODO déclarations nécessaires : mutex, ...
    int result = 0;
    float * tab = ut_generateTab(data->nb, data->min, data->max, 0);
    pthread_mutex_t mutex;
    pthread_mutex_init(&mutex, NULL);

    //TODO lancement des threads
    pthread_t multithread[data->nbThreads];
    Thread thread[data->nbThreads];
    int nbEltThread = data->nb / data->nbThreads;

    for(int i=0; i<data->nbThreads; i++){
        thread[i].tab = tab+i*nbEltThread;
        thread[i].nbElt = nbEltThread;
        thread[i].res = &result;
        thread[i].mutex = &mutex;

        pthread_create(&multithread[i], NULL, MultiThread, (void*)&thread[i]);
    }

    //TODO attente de la fin des threads
    for(int i=0; i<data->nbThreads; i++){
        pthread_join(multithread[i], NULL);
    }
    pthread_mutex_destroy(&mutex);

    // résultat (result a été rempli par les threads)
    // affichage du tableau si pas trop gros
    if (data->nb <= 20)
    {
        printf("[");
        for (int i = 0; i < data->nb; i++)
        {
            if (i != 0)
                printf(" ");
            printf("%g", tab[i]);
        }
        printf("]\n");
    }
    // recherche linéaire pour vérifier
    int nbVerif = 0;
    for (int i = 0; i < data->nb; i++)
    {
        if (tab[i] == data->elt)
            nbVerif ++;
    }
    printf("Elément %g présent %d fois (%d attendu)\n", data->elt, result, nbVerif);
    if (result == nbVerif)
        printf("=> ok ! le résultat calculé par les threads est correct\n");
    else
        printf("=> PB ! le résultat calculé par les threads est incorrect\n");

    //TODO libération des ressources
    free(tab);    
}


/************************************************************************
 * Partie communication avec le master
 ************************************************************************/
// envoi des données au master
//j ai enleve le const
void sendData(Data *data)
{
    //TODO
    // - envoi de l'ordre au master (cf. CM_ORDER_* dans client_master.h)
    // - envoi des paramètres supplémentaires au master (pour CM_ORDER_EXIST,
    //   CM_ORDER_INSERT et CM_ORDER_INSERT_MANY)
    int ret;

    data->client_to_master = open(CLIENT_TO_MASTER, O_WRONLY);
    myassert(data->client_to_master != -1, "erreur tube non crée");

    ret = write(data->client_to_master, &(data->order), sizeof(int));
    myassert(ret != -1, "erreur");

    if(data->order == CM_ORDER_EXIST || data->order == CM_ORDER_INSERT){  
        ret = write(data->client_to_master, &(data->elt), sizeof(float));
        myassert(ret != -1, "erreur");  
    }
    else if(data->order == CM_ORDER_INSERT_MANY){
        ret = write(data->client_to_master, &(data->nb), sizeof(int));
        myassert(ret != -1, "erreur"); 

        ret = write(data->client_to_master, &(data->min), sizeof(float));
        myassert(ret != -1, "erreur");

        ret = write(data->client_to_master, &(data->max), sizeof(float));
        myassert(ret != -1, "erreur");
    } 

    ret = close(data->client_to_master);
    myassert(ret != -1, "erreur");
    //END TODO
}

// attente de la réponse du master
//j ai enlever le const
void receiveAnswer(Data *data)
{
    //TODO
    // - récupération de l'accusé de réception du master (cf. CM_ANSWER_* dans client_master.h)
    // - selon l'ordre et l'accusé de réception :
    //      . récupération de données supplémentaires du master si nécessaire
    // - affichage du résultat
    int ret;

    data->master_to_client = open(MASTER_TO_CLIENT, O_RDONLY);
    myassert(data->master_to_client != -1, "erreur tube non crée");

    ret = read(data->client_to_master, &data->answer, sizeof(int));
    myassert(ret != -1, "erreur");  
    printf("la valeur recu est : %d \n", data->answer);

    if(data->answer == 0){
        printf("arret effectue \n");
    }
    else if(data->answer == 21 || data->answer == 31){
        printf("ensemble vide \n");
    }
    else if(data->answer == 10){
        int how_many;
        int how_many_distinct;

        ret = read(data->client_to_master, &how_many, sizeof(int));
        myassert(ret != -1, "erreur");  
        printf("le nombre d'élément : %d \n", how_many);

        ret = read(data->client_to_master, &how_many_distinct, sizeof(int));
        myassert(ret != -1, "erreur");  
        printf("le nombre d'élément distinct : %d \n", how_many_distinct);
    }
    else if(data->answer == 20){
        int min;

        ret = read(data->client_to_master, &min, sizeof(int));
        myassert(ret != -1, "erreur");  
        printf("le minimum est : %d \n", min);
    }
    else if(data->answer == 30){
        int max;

        ret = read(data->client_to_master, &max, sizeof(int));
        myassert(ret != -1, "erreur");  
        printf("le maximum est : %d \n", max);
    }
    else if(data->answer == 40){
        printf("l'element %f existe : \n", data->elt);
    }
    else if(data->answer == 41){
        printf("l'element %f n'existe pas : \n", data->elt);
    }
    else if(data->answer == 50){
        int somme;

        ret = read(data->client_to_master, &somme, sizeof(int));
        myassert(ret != -1, "erreur");  
        printf("la somme est  : %d \n", somme);
    }
    else if(data->answer == 60 || data->answer == 70){
        printf("insertion effectue \n");
    }
    else if(data->answer == 80){
        printf("affichage effectue \n");
    }
    else{
        printf("reponse non valide \n");
    }
    ret = close(data->client_to_master);
    myassert(ret != -1, "erreur \n");
    //END TODO
}


/************************************************************************
 * Fonction principale
 ************************************************************************/
int main(int argc, char * argv[])
{
    Data data;
    parseArgs(argc, argv, &data);

    if (data.order == CM_ORDER_LOCAL)
        lauchThreads(&data);
    else
    {
        //TODO
        // - entrer en section critique :
        //       . pour empêcher que 2 clients communiquent simultanément
        //       . le mutex est déjà créé par le master
        // - ouvrir les tubes nommés (ils sont déjà créés par le master)
        //       . les ouvertures sont bloquantes, il faut s'assurer que
        //         le master ouvre les tubes dans le même ordre
        int semId;

        semId = my_semget("client_master.h", 4);

        entrerSC(semId);
        //END TODO

        sendData(&data);
        receiveAnswer(&data);
        sleep(10);

        //TODO
        // - sortir de la section critique
        sortirSC(semId);
        // - libérer les ressources (fermeture des tubes, ...)
        // - débloquer le master grâce à un second sémaphore (cf. ci-dessous)
        //
        // Une fois que le master a envoyé la réponse au client, il se bloque
        // sur un sémaphore ; le dernier point permet donc au master de continuer
        //
        // N'hésitez pas à faire des fonctions annexes ; si la fonction main
        // ne dépassait pas une trentaine de lignes, ce serait bien.
    }
    
    return EXIT_SUCCESS;
}
