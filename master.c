#if defined HAVE_CONFIG_H
#include "config.h"
#endif

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "utils.h"
#include "myassert.h"

#include "client_master.h"
#include "master_worker.h"

/************************************************************************
 * Données persistantes d'un master
 ************************************************************************/
typedef struct
{
    // communication avec le client
    int client_to_master;
    int master_to_client;
    // données internes
    bool existWorker;
    int order;
    float elt;
    int nb;
    float min;     
    float max;
    // communication avec le premier worker (double tubes)
    int master_to_first_worker_lecture;
    int master_to_first_worker_ecriture;

    int first_worker_to_master_lecture;
    int first_worker_to_master_ecriture;
    // communication en provenance de tous les workers (un seul tube en lecture)
    int worker_to_master_lecture;
    int worker_to_master_ecriture;
    //TODO
} Data;


/************************************************************************
 * Usage et analyse des arguments passés en ligne de commande
 ************************************************************************/
static void usage(const char *exeName, const char *message)
{
    fprintf(stderr, "usage : %s\n", exeName);
    if (message != NULL)
        fprintf(stderr, "message : %s\n", message);
    exit(EXIT_FAILURE);
}


/************************************************************************
 * initialisation complète
 ************************************************************************/
void init(Data *data)
{
    myassert(data != NULL, "il faut l'environnement d'exécution");

    //TODO initialisation data
    int ret;

    data->existWorker = false;

    ret = mkfifo(CLIENT_TO_MASTER, 0600);
    myassert(ret != -1, "erreur tube non crée");
    data->client_to_master = -1;

    ret = mkfifo(MASTER_TO_CLIENT, 0600);
    myassert(ret != -1, "erreur tube non crée");
    data->master_to_client = -1;

    int m_to_fw[2];
    ret = pipe(m_to_fw);
    myassert(ret != -1, "erreur tube non crée");
    data->master_to_first_worker_ecriture = m_to_fw[1];
    data->master_to_first_worker_lecture = m_to_fw[0];

    int fw_to_m[2];
    ret = pipe(fw_to_m);
    myassert(ret != -1, "erreur tube non crée");
    data->first_worker_to_master_lecture = fw_to_m[0];
    data->first_worker_to_master_ecriture = fw_to_m[1];

    int w_to_m[2];
    ret = pipe(w_to_m);
    myassert(ret != -1, "erreur tube non crée");
    data->worker_to_master_lecture = w_to_m[0];
    data->worker_to_master_ecriture = w_to_m[1];

    //pensez rajouter de quoi conserve l'autre sortie des tube anonime
}


/************************************************************************
 * fin du master
 ************************************************************************/
void orderStop(Data *data)
{
    TRACE0("[master] ordre stop\n");
    myassert(data != NULL, "il faut l'environnement d'exécution");

    //TODO
    // - traiter le cas ensemble vide (pas de premier worker)
    // - envoyer au premier worker ordre de fin (cf. master_worker.h)
    // - attendre sa fin
    // - envoyer l'accusé de réception au client (cf. client_master.h)
    int ret;

    if(data->existWorker == true){
        write(data->master_to_first_worker_ecriture, &data->order, sizeof(int));
        close(data->master_to_first_worker_ecriture);
    }
    int answer = 0;
    ret = write(data->master_to_client, &answer, sizeof(int));
    myassert(ret != -1, "erreur");
    //END TODO
}


/************************************************************************
 * quel est la cardinalité de l'ensemble
 ************************************************************************/
void orderHowMany(Data *data)
{
    TRACE0("[master] ordre how many\n");
    myassert(data != NULL, "il faut l'environnement d'exécution");

    //TODO
    // - traiter le cas ensemble vide (pas de premier worker)
    // - envoyer au premier worker ordre howmany (cf. master_worker.h)
    // - recevoir accusé de réception venant du premier worker (cf. master_worker.h)
    // - recevoir résultats (deux quantités) venant du premier worker
    // - envoyer l'accusé de réception au client (cf. client_master.h)
    // - envoyer les résultats au client
    //END TODO
}


/************************************************************************
 * quel est la minimum de l'ensemble
 ************************************************************************/
void orderMinimum(Data *data)
{
    TRACE0("[master] ordre minimum\n");
    myassert(data != NULL, "il faut l'environnement d'exécution");

    //TODO
    // - si ensemble vide (pas de premier worker)
    //       . envoyer l'accusé de réception dédié au client (cf. client_master.h)
    // - sinon
    //       . envoyer au premier worker ordre minimum (cf. master_worker.h)
    //       . recevoir accusé de réception venant du worker concerné (cf. master_worker.h)
    //       . recevoir résultat (la valeur) venant du worker concerné
    //       . envoyer l'accusé de réception au client (cf. client_master.h)
    //       . envoyer le résultat au client
    //END TODO
}


/************************************************************************
 * quel est la maximum de l'ensemble
 ************************************************************************/
void orderMaximum(Data *data)
{
    TRACE0("[master] ordre maximum\n");
    myassert(data != NULL, "il faut l'environnement d'exécution");

    //TODO
    // cf. explications pour le minimum
    //END TODO
}


/************************************************************************
 * test d'existence
 ************************************************************************/
void orderExist(Data *data)
{
    TRACE0("[master] ordre existence\n");
    myassert(data != NULL, "il faut l'environnement d'exécution");

    //TODO
    // - recevoir l'élément à tester en provenance du client
    // - si ensemble vide (pas de premier worker)
    //       . envoyer l'accusé de réception dédié au client (cf. client_master.h)
    // - sinon
    //       . envoyer au premier worker ordre existence (cf. master_worker.h)
    //       . envoyer au premier worker l'élément à tester
    //       . recevoir accusé de réception venant du worker concerné (cf. master_worker.h)
    //       . si élément non présent
    //             . envoyer l'accusé de réception dédié au client (cf. client_master.h)
    //       . sinon
    //             . recevoir résultat (une quantité) venant du worker concerné
    //             . envoyer l'accusé de réception au client (cf. client_master.h)
    //             . envoyer le résultat au client
    int ret;

    ret = read(data->client_to_master, &(data->elt), sizeof(float));
        myassert(ret != -1, "erreur");
        printf("le elt recu est : %f \n", data->elt);

    int answer = 41;
    ret = write(data->master_to_client, &answer, sizeof(int));
    myassert(ret != -1, "erreur");
    //END TODO
}

/************************************************************************
 * somme
 ************************************************************************/
void orderSum(Data *data)
{
    TRACE0("[master] ordre somme\n");
    myassert(data != NULL, "il faut l'environnement d'exécution");

    //TODO
    // - traiter le cas ensemble vide (pas de premier worker) : la somme est alors 0
    // - envoyer au premier worker ordre sum (cf. master_worker.h)
    // - recevoir accusé de réception venant du premier worker (cf. master_worker.h)
    // - recevoir résultat (la somme) venant du premier worker
    // - envoyer l'accusé de réception au client (cf. client_master.h)
    // - envoyer le résultat au client
    //END TODO
}

/************************************************************************
 * insertion d'un élément
 ************************************************************************/

//TODO voir si une fonction annexe commune à orderInsert et orderInsertMany est justifiée

void orderInsert(Data *data)
{
    TRACE0("[master] ordre insertion\n");
    myassert(data != NULL, "il faut l'environnement d'exécution");

    //TODO
    // - recevoir l'élément à insérer en provenance du client
    // - si ensemble vide (pas de premier worker)
    //       . créer le premier worker avec l'élément reçu du client
    // - sinon
    //       . envoyer au premier worker ordre insertion (cf. master_worker.h)
    //       . envoyer au premier worker l'élément à insérer
    // - recevoir accusé de réception venant du worker concerné (cf. master_worker.h)
    // - envoyer l'accusé de réception au client (cf. client_master.h)
    int ret;

    ret = read(data->client_to_master, &(data->elt), sizeof(float));
    myassert(ret != -1, "erreur");
    printf("le elt recu est : %f \n", data->elt);

    if(data->existWorker == false){
        pid_t pid = fork();
        if (pid == -1) {
            perror("Fork failed");
            exit(EXIT_FAILURE);
        } 
        else if (pid == 0) {
            ret = close(data->master_to_first_worker_ecriture);
            myassert(ret != -1, "erreur");
            ret = close(data->first_worker_to_master_lecture);
            myassert(ret != -1, "erreur");
            ret = close(data->worker_to_master_lecture);

            char elt[100];
            char master_to_first_worker[100];
            char first_worker_to_master[100];
            char worker_to_master[100];

            sprintf(elt, "%f", data->elt);
            sprintf(master_to_first_worker, "%d", data->master_to_first_worker_lecture);
            sprintf(first_worker_to_master, "%d", data->first_worker_to_master_ecriture);
            sprintf(worker_to_master, "%d", data->worker_to_master_ecriture);

            char *args[5] = {"./worker", elt, master_to_first_worker, first_worker_to_master, worker_to_master};
            execv(args[0], args);
            write(data->master_to_first_worker_ecriture, &data->order, sizeof(int));
            perror("Execv failed");
            exit(EXIT_FAILURE);
        } 
        data->existWorker = true;
    }
    else{
        ret = close(data->master_to_first_worker_lecture);
        myassert(ret != -1, "erreur");
        ret = close(data->first_worker_to_master_ecriture);
        myassert(ret != -1, "erreur");
        ret = close(data->first_worker_to_master_ecriture);
        myassert(ret != -1, "erreur");

        write(data->master_to_first_worker_ecriture, &data->order, sizeof(int));
        write(data->master_to_first_worker_ecriture, &data->elt, sizeof(float));
        close(data->master_to_first_worker_ecriture);
    }

    //END TODO
}


/************************************************************************
 * insertion d'un tableau d'éléments
 ************************************************************************/
void orderInsertMany(Data *data)
{
    TRACE0("[master] ordre insertion tableau\n");
    myassert(data != NULL, "il faut l'environnement d'exécution");

    //TODO
    // - recevoir le tableau d'éléments à insérer en provenance du client
    // - pour chaque élément du tableau
    //       . l'insérer selon l'algo vu dans orderInsert (penser à factoriser le code)
    // - envoyer l'accusé de réception au client (cf. client_master.h)
    int ret;
    
    ret = read(data->client_to_master, &(data->nb), sizeof(int));
        myassert(ret != -1, "erreur");
        printf("le nb recu est : %d \n", data->nb);

        ret = read(data->client_to_master, &(data->min), sizeof(float));
        myassert(ret != -1, "erreur");
        printf("le min recu est : %f \n", data->min);

        ret = read(data->client_to_master, &(data->max), sizeof(float));
        myassert(ret != -1, "erreur");
        printf("le max recu est : %f \n", data->max);
    //END TODO
}


/************************************************************************
 * affichage ordonné
 ************************************************************************/
void orderPrint(Data *data)
{
    TRACE0("[master] ordre affichage\n");
    myassert(data != NULL, "il faut l'environnement d'exécution");

    //TODO
    // - traiter le cas ensemble vide (pas de premier worker)
    // - envoyer au premier worker ordre print (cf. master_worker.h)
    // - recevoir accusé de réception venant du premier worker (cf. master_worker.h)
    //   note : ce sont les workers qui font les affichages
    // - envoyer l'accusé de réception au client (cf. client_master.h)
    //END TODO
}


/************************************************************************
 * boucle principale de communication avec le client
 ************************************************************************/
void loop(Data *data)
{
    bool end = false;

    while (!end)
    {
        int ret;
        //TODO ouverture des tubes avec le client (cf. explications dans client.c)
        data->client_to_master = open(CLIENT_TO_MASTER, O_RDONLY);
        myassert(data->client_to_master != -1, "erreur tube non crée");
        data->master_to_client = open(MASTER_TO_CLIENT, O_WRONLY);
        myassert(data->master_to_client != -1, "erreur tube non crée");

        ret = read(data->client_to_master, &(data->order), sizeof(int));
        myassert(ret != -1, "erreur");
        printf("l ordre recu est : %d \n", data->order);

        switch(data->order)
        {
          case CM_ORDER_STOP:
            orderStop(data);
            end = true;
            break;
          case CM_ORDER_HOW_MANY:
            orderHowMany(data);
            break;
          case CM_ORDER_MINIMUM:
            orderMinimum(data);
            break;
          case CM_ORDER_MAXIMUM:
            orderMaximum(data);
            break;
          case CM_ORDER_EXIST:
            orderExist(data);
            break;
          case CM_ORDER_SUM:
            orderSum(data);
            break;
          case CM_ORDER_INSERT:
            orderInsert(data);
            break;
          case CM_ORDER_INSERT_MANY:
            orderInsertMany(data);
            break;
          case CM_ORDER_PRINT:
            orderPrint(data);
            break;
          default:
            myassert(false, "ordre inconnu");
            exit(EXIT_FAILURE);
            break;
        }

        //TODO fermer les tubes nommés
        //     il est important d'ouvrir et fermer les tubes nommés à chaque itération
        //     voyez-vous pourquoi ?
        ret = close(data->client_to_master);
        myassert(ret != -1, "erreur");
        //ret = close(data->master_to_client);
        //myassert(ret != -1, "erreur");
        //TODO attendre ordre du client avant de continuer (sémaphore pour une précédence)

        TRACE0("[master] fin ordre\n");
    }
}


/************************************************************************
 * Fonction principale
 ************************************************************************/

//TODO N'hésitez pas à faire des fonctions annexes ; si les fonctions main
//TODO et loop pouvaient être "courtes", ce serait bien

int main(int argc, char * argv[])
{
    if (argc != 1)
        usage(argv[0], NULL);

    TRACE0("[master] début\n");

    Data data;

    //TODO
    // - création des sémaphores
    int sema_Client = create_semaphore(1,"client_master.h", 4);
    // - création des tubes nommés
    init(&data);
    //END TODO

    loop(&data);

    //TODO destruction des tubes nommés, des sémaphores, ...
    int ret;
    ret = unlink(MASTER_TO_CLIENT);
    myassert(ret != -1, "erreur");
    ret = unlink(CLIENT_TO_MASTER);
    myassert(ret != -1, "erreur");
    my_destroy(sema_Client);
    TRACE0("[master] terminaison\n");
    return EXIT_SUCCESS;
}
